package com.bjdkf.os.core.file;

import java.util.ArrayList;

// Create by lijundi
// 2019/08/29
public class Directory {
    private String directoryName;
    private int directoryId;
    private Directory parentDirectory;
    private ArrayList<File> fileList;
    private ArrayList<Directory> directoryList;

    public Directory(String directoryName, Directory parentDirectory) {
        this.directoryName = directoryName;
        this.parentDirectory = parentDirectory;
        this.fileList = new ArrayList<File>();
        this.directoryList = new ArrayList<Directory>();
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public ArrayList<File> getFileList() {
        return fileList;
    }

    public ArrayList<Directory> getDirectoryList() {
        return directoryList;
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public void addFile(File file) {
        this.fileList.add(file);
    }

    public void delFile(File file) {
        this.fileList.remove(file);
    }

    public void addDirectory(Directory directory) {
        this.directoryList.add(directory);
    }

    public void delDirectory(Directory directory) {
        this.directoryList.remove(directory);
    }

    public File getFile(String fileName) {
        for (File f : fileList) {
            if (f.getFileName().equals(fileName)) {
                return f;
            }
        }
        return null;
    }

    public Directory getDirectory(String directoryName) {
        for (Directory d : directoryList) {
            if (d.getDirectoryName().equals(directoryName)) {
                return d;
            }
        }
        return null;
    }
}
