package com.bjdkf.os.core.file;

import com.bjdkf.os.ds.Page;

import java.util.ArrayList;

// Create by lijundi
// 2019/08/29
public class File {
    private int fileId;
    private String fileName;
    private int type;//0--普通文件，1--程序文件
    private String address;//程序的实际地址
    private ArrayList<Page> memoryPages;
    private ArrayList<Page> storagePages;

    public File(String fileName, int type, ArrayList<Page> storagePages) {
        this.fileName = fileName;
        this.type = type;
        this.memoryPages = null;
        this.storagePages = storagePages;
        this.address = null;
    }

    public File(String fileName, int type, ArrayList<Page> storagePages, String address) {
        this.fileName = fileName;
        this.type = type;
        this.memoryPages = null;
        this.storagePages = storagePages;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public String getFileName() {
        return fileName;
    }

    public int getType() {
        return type;
    }

    public int getSize() {
        if (memoryPages == null)
            return storagePages.size();
        else
            return memoryPages.size();
    }

    public ArrayList<Page> getMemoryPages() {
        return memoryPages;
    }

    public ArrayList<Page> getStoragePages() {
        return storagePages;
    }

    public void setMemoryPages(ArrayList<Page> memoryPages) {
        this.memoryPages = memoryPages;
    }

    public void setStoragePages(ArrayList<Page> storagePages) {
        this.storagePages = storagePages;
    }
}
