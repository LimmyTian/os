package com.bjdkf.os.core.file;

import com.bjdkf.os.ds.PageStore;
import com.bjdkf.os.ds.Page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

// Create by lijundi
// 2019/08/29
public class Storage extends PageStore {
    private Directory rootDir; // 根目录节点
    private Directory currentDir; //当前目录节点

    public Storage(int pageNum) {
        super(pageNum);
        rootDir = new Directory("root",null);
    }

    public Directory getRootDir() {
        return rootDir;
    }

    public Directory getCurrentDir() {
        return currentDir;
    }

    public void setCurrentDir(Directory currentDir) {
        this.currentDir = currentDir;
    }

    public void createFile(String fileName, int size){
        ArrayList<Page> pages = this.loadBySize(size);
        this.currentDir.addFile(new File(fileName, 0, pages));
    }

    public void createProgram(String fileName, int size, String address){
        ArrayList<Page> pages = this.loadBySize(size);
        this.currentDir.addFile(new File(fileName, 1, pages, address));
    }

    public void createDirectory(String directoryName){
        this.currentDir.addDirectory(new Directory(directoryName, this.currentDir));
    }

    public void delFile(String fileName){
        File file = this.currentDir.getFile(fileName);
        this.releasePage(file.getStoragePages());
        this.currentDir.delFile(file);
    }

    public void delDirectory(String directoryName){
        this.currentDir.delDirectory(this.currentDir.getDirectory(directoryName));
    }

    public File getFileByAddress(String address){
        Directory tmpDir = this.rootDir;
        File file = null;
        String[] strings = address.split("/");
        for (int i = 1; i < strings.length; i++) {
            if(i == strings.length-1){
                file = tmpDir.getFile(strings[i]);
                break;
            }
            tmpDir = tmpDir.getDirectory(strings[i]);
        }
        return file;
    }

    public String getCurrentAddress(){
        String address = this.currentDir.getDirectoryName() + ">";
        if(!this.currentDir.equals(this.rootDir)){ // 不等于当前目录
            Directory tmpDir = this.currentDir;
            do {
                tmpDir = tmpDir.getParentDirectory();
                String tmpString = tmpDir.getDirectoryName() + "\\";
                address = tmpString.concat(address);
            }while(!tmpDir.equals(this.rootDir));
        }
        return address;
    }

    public Directory createDirectoryByAddress(String[] directories){
        Directory currentDir = this.rootDir;
        for (int i = 0; i < directories.length; i++) {
            ArrayList<Directory> directoryArrayList = currentDir.getDirectoryList();
            boolean flag = false;
            for (Directory dir:
                 directoryArrayList) {
                if(dir.getDirectoryName().equals(directories[i])){
                    currentDir = dir;
                    flag = true;
                    break;
                }
            }
            if(!flag){//不存在该目录则创建
                Directory newDirectory = new Directory(directories[i], currentDir);
                currentDir.addDirectory(newDirectory);
                currentDir = newDirectory;
            }
        }
        return currentDir;
    }

    public void createFileByAddress(String[] files){
        ArrayList<String> list= new ArrayList<String>();
        String file = null;
        for (int i = 0; i < files.length; i++) {
            if(i == files.length - 1){
                file = files[i];
                break;
            }
            list.add(files[i]);
        }
        String[] dirs = new String[list.size()];
        list.toArray(dirs);
        //设置当前目录，直接调函数
        this.currentDir = this.createDirectoryByAddress(dirs);
        String[] fileAndSize = file.split(" ");
        this.createFile(fileAndSize[0], Integer.parseInt(fileAndSize[1]));
        //当前目录还原回根目录
        this.currentDir = this.rootDir;
    }
}
