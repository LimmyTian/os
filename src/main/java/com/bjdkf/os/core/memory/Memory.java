package com.bjdkf.os.core.memory;

import java.util.ArrayList;

import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.file.Storage;
import com.bjdkf.os.ds.PageStore;
import com.bjdkf.os.ds.Page;
import com.bjdkf.os.glb.Global;

// Create by lijundi
// 2019/08/29
public class Memory extends PageStore {
    public Memory(int pageNum) {
        super(pageNum);
    }

    //返回成功或失败
    public boolean loadFile(File file) {
        ArrayList<Page> pages = this.loadBySize(file.getSize());
        if(pages==null){
            return false;
        }
        else{
            file.setMemoryPages(pages);
            return true;
        }
    }

    public boolean closeFile(File file){
        // 更新存储器
        if (file.getMemoryPages().size() != file.getStoragePages().size()) {
            Global.storage.releasePage(file.getStoragePages());//释放存储器空间
            file.setStoragePages(Global.storage.loadBySize(file.getMemoryPages().size()));
        }
        // 释放内存
        this.releasePage(file.getMemoryPages());
        file.setMemoryPages(null);
        return true;
    }

    public boolean writeFile(File file, int size){
        this.releasePage(file.getMemoryPages());
        ArrayList<Page> pages = this.loadBySize(size);
        if(pages==null){
            return false;
        }
        else{
            file.setMemoryPages(pages);
            return true;
        }
    }
}
