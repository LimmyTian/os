package com.bjdkf.os.core.process;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.device.Interrupt;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.glb.Global;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author lijundi
 * @date 2019/9/3 13:16
 */
public class Actuator implements Runnable {
    private Process process;
    private Thread t;
    private boolean runningFlag = true;

    public Actuator(Process process) {
        this.process = process;
    }

    public void run() {
        Instant instantStart = Instant.now();
        int difference = 0;
        do {
            //处理中断
            interruptProcess();
            //获取代码
            String code = this.getProcessCode(this.process);
            System.out.println("[" + this.process.getCodeFile().getFileName() + "] " + "准备执行：" + code);
            //执行代码
            executeProcessCode(CodeInterpreter.interpreting(code));
            //计算使用时间
            Instant instantEnd = Instant.now();
            difference = (int) Duration.between(instantStart, instantEnd).getSeconds();
            if(runningFlag){
                if (difference > Global.duration) {
                    Scheduler.readyAndSchedule(this.process);
                    runningFlag = false;
                }
            }
        } while (runningFlag);//结束进程
    }

    public void start() {
        if (t == null) {
            t = new Thread(this, "Actuator");
            t.start();
        }
    }

    /**
     * 中断处理
     */
    public static void interruptProcess() {
        while (!Global.interruptArrayList.isEmpty()) {
            System.out.println("        中断处理");
            Interrupt interrupt = Global.interruptArrayList.get(0);
            interrupt.getProcess().releaseDevice(interrupt.getDevice());
            Global.interruptArrayList.remove(0);
        }
    }

    /**
     * 获取代码
     */
    public String getProcessCode(Process process) {
        try {
            java.io.File file = new java.io.File(process.getCodeFile().getAddress());
            BufferedReader br = new BufferedReader(new FileReader(file));
            int pc = process.getPc() + 3;
            String code = null;
            for (int i = 0; i < pc; i++) {
                code = br.readLine();
            }
            return code;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 代码执行
     */
    public void executeProcessCode(ArrayList<String> code) {
        try {
            switch (code.get(0)) {
                case "S":
                    Thread.sleep(Integer.parseInt(code.get(1))*1000);//毫秒
                    process.incPc();
                    break;
                case "R":
                    File rFile = Global.storage.getFileByAddress(code.get(1));
                    if (rFile != null) {
                        if (this.process.openFile(rFile)) {
                            Thread.sleep(Integer.parseInt(code.get(2))*1000);//毫秒
                            process.incPc();
                        } else {//内存不足
                            Scheduler.readyAndSchedule(this.process);
                            this.runningFlag = false;
                        }
                    }
                    break;
                case "W":
                    File wFile = Global.storage.getFileByAddress(code.get(1));
                    if (wFile != null) {
                        if (Global.memory.writeFile(wFile, Integer.parseInt(code.get(2)))) {
                            Thread.sleep(Integer.parseInt(code.get(3))*1000);//毫秒
                            process.incPc();
                        } else {//内存不足
                            Scheduler.readyAndSchedule(this.process);
                            this.runningFlag = false;
                        }
                    }
                    break;
                case "D":
                    Device device = Global.DEVICES.get(code.get(1));
                    if (this.process.getAllocatedDevice().contains(device) || device.aquire(this.process)) {
                        //设备已就绪
                        //定时器，延时几秒，然后添加interrupt
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            public void run() {
                                Global.interruptArrayList.add(new Interrupt(device, process));
                            }
                        }, 1000 * Integer.parseInt(code.get(2)));
                        process.incPc();
                    } else {
                        //设备未就绪
                        Scheduler.wait(this.process);
                        this.runningFlag = false;
                    }
                    break;
                case "P":
                    if (Synchronizator.getSemaphore(code.get(1)) == null) {
                        Synchronizator.putDefaultSemaphore(code.get(1));
                    }
                    if (Synchronizator.getSemaphore(code.get(1)).get(1).tryAcquire()) {
                        Synchronizator.getSemaphore(code.get(1)).get(1).release();
                        Thread.sleep(Integer.parseInt(code.get(2))*1000);//毫秒
                        Synchronizator.getSemaphore(code.get(1)).get(1).tryAcquire();
                        process.incPc();
                        Synchronizator.getSemaphore(code.get(1)).get(0).release();
                    } else {
                        Scheduler.readyAndSchedule(this.process);
                        this.runningFlag = false;
                    }
                    break;
                case "C":
                    if (Synchronizator.getSemaphore(code.get(1)) == null) {
                        Synchronizator.putDefaultSemaphore(code.get(1));
                    }
                    if (Synchronizator.getSemaphore(code.get(1)).get(0).tryAcquire()) {
                        Synchronizator.getSemaphore(code.get(1)).get(0).release();
                        Thread.sleep(Integer.parseInt(code.get(2))*1000);//毫秒
                        Synchronizator.getSemaphore(code.get(1)).get(0).tryAcquire();
                        process.incPc();
                        Synchronizator.getSemaphore(code.get(1)).get(1).release();
                    } else {
                        Scheduler.readyAndSchedule(this.process);
                        this.runningFlag = false;
                    }
                    break;
                case "Q":
                    if(this.process.getAllocatedDevice().isEmpty()){
                        Scheduler.killAndSchedule(this.process);
                        this.runningFlag = false;
                    }
                    else {//设备还在使用
                        Scheduler.readyAndSchedule(this.process);
                        this.runningFlag = false;
                    }
                    break;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
