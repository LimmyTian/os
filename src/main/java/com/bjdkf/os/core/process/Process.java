package com.bjdkf.os.core.process;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.ds.ProcessState;
import com.bjdkf.os.glb.Global;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Limmy
 * @date 2019-08-29 11:04
 */
public class Process {
    /**
     * 下一个进程编号
     */
    private static int nextProcess = 1;

    private boolean swap = false;
    private int pid;
    /**
     * 已经执行完的行数
     */
    private int pc = 0;
    /**
     * 优先级
     */
    private int priority;
    private List<Device> allocatedDevices = new ArrayList<Device>();
    private ProcessState state = ProcessState.NEW;
    /**
     * 程序文件
     */
    private File codeFile;

    /**
     * 打开文件表
     */
    private List<File> openedFiles = new ArrayList<File>();

    private Process() {
    }

    private synchronized void allocatePID() {
        pid = nextProcess;
        nextProcess++;
    }

    public Process(File codeFile, int priority) {
        allocatePID();
        this.codeFile = codeFile;
        this.priority = priority;
        // 申请内存扔，如果分配不下，就swapOut DIDI
        int i = 0;
        for (; i < 10 && !Global.memory.loadFile(codeFile); i++) {
            Scheduler.swapOut();
        }
        if (i == 10) {
            System.out.println("内存不足，创建进程失败");
            return;
        }
        Global.processes.put(pid, this);
    }

    public int getPriority() {
        return priority;
    }

    public File getCodeFile() {
        return codeFile;
    }

    public int getPc() {
        return pc;
    }

    public void incPc() {
        this.pc = pc + 1;
    }

    public void kill() {
        // 释放程序文件
        Global.memory.closeFile(this.codeFile);
        // 释放文件 DIDI
        while(!openedFiles.isEmpty()) {
            closeFile(openedFiles.get(0));
        }
        // 释放设备
        while(!allocatedDevices.isEmpty()) {
            releaseDevice(allocatedDevices.get(0));
        }
        Global.processes.remove(pid);
        System.out.println("[" + codeFile.getFileName() + "] " + "进程结束");
    }

    public int getPid() {
        return pid;
    }

    public void setState(ProcessState state) {
        this.state = state;
    }

    public boolean openFile(File file) {
        // 更新内存信息 DIDI
        int i = 0;
        while (i < 3 && !Global.memory.loadFile(file)) {
            Scheduler.swapOut();
            i++;
        }
        if (i != 3) {
            openedFiles.add(file);
            System.out.println("[" + codeFile.getFileName() + "] " + "打开文件：" + file.getFileName());
            return true;
        }
        return false;
    }

    public void closeFile(File file) {
        openedFiles.remove(file);
        // 释放占用内存 DIDI
        Global.memory.closeFile(file);
    }

    public List<Device> getAllocatedDevice() {
        return allocatedDevices;
    }

    public List<File> getOpenedFiles() {
        return openedFiles;
    }

    public void useDevice(Device device) {
        System.out.println("[" + codeFile.getFileName() + "] " + "获取到设备" + device.getName());
        allocatedDevices.add(device);
    }

    public void releaseDevice(Device device) {
        if (allocatedDevices.isEmpty()) {
            return;
        }
        allocatedDevices.get(allocatedDevices.indexOf(device)).release();
        allocatedDevices.remove(device);
        System.out.println("[" + codeFile.getFileName() + "] " + "释放设备" + device.getName());
    }

    public void incPriority() {
        priority++;
    }

    public int getSize() {
        return codeFile.getMemoryPages().size() + openedFiles.size();
    }

    public void setSwap(boolean swap) {
        this.swap = swap;
    }

    public boolean getSwap() {
        return swap;
    }

    public ProcessState getState() {
        return state;
    }
}
