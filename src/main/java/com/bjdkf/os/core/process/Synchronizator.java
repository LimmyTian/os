package com.bjdkf.os.core.process;

import com.bjdkf.os.glb.Global;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

/**
 * @author Limmy
 * @date 2019-09-02 22:49
 */
public class Synchronizator {
    /**
     * 维护信号量的 HashMap ,
     */
    private static Map<String, List<Semaphore>> semaphoreMap = new HashMap<String, List<Semaphore>>();

    public static List<Semaphore> getSemaphore(String name) {
        return semaphoreMap.get(name);
    }

    /**
     * 更新/插入信号量
     */
    public static void putDefaultSemaphore(String name) {
        semaphoreMap.put(name, new ArrayList<Semaphore>(){{
            add(new Semaphore(Global.full));
            add(new Semaphore(Global.empty));
        }});
    }

    public static void putIfAbsent(String name, int number) {
        semaphoreMap.putIfAbsent(name, new ArrayList<Semaphore>(){{
            add(new Semaphore(Global.full));
            add(new Semaphore(number));
        }});
    }

    /**
     * 获取信号量们
     */
    public static Map<String, List<Semaphore>> getSemaphoreMap() {
        return semaphoreMap;
    }
}
