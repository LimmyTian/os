### 代码

- 属性代码
  - M size  进程占用内存空间，页数为size  
  - Y priority  进程的优先数为priority
- 执行代码
  - S time  计算指令 时长为time秒
  - R filename time 读文件指令，文件名为filename，时长为time秒
  - W filename size time 写文件指令，文件名为filename，时长为time秒  
  - D devicename time 申请设备指令，设备名为devicename，时长为time秒
  - P resourcename time  生产资源指令，资源名为resourcename，时长为time秒
  - C resourcename time  消费资源指令，资源名为resourcename，时长为time秒
  - Q 结束运行指令