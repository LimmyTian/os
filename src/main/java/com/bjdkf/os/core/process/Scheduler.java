package com.bjdkf.os.core.process;

import com.bjdkf.os.core.file.File;
import com.bjdkf.os.ds.ProcessState;
import com.bjdkf.os.glb.Global;
import com.bjdkf.os.core.process.Process;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Limmy
 * @date 2019-08-29 20:29
 */
public class Scheduler {
    private static List<Process> readyList = new ArrayList<>();
    private static List<Process> waitingList = new ArrayList<>();
    private static Process runningProcess;

    public static void ready(Process process) {
        waitingList.remove(process);
        readyList.add(process);
        process.setState(ProcessState.READY);
        System.out.println("[" + process.getCodeFile().getFileName() + "] " + "进程进入 Ready 状态");
        readyList = readyList.stream()
                .sorted(Comparator.comparing(Process::getPriority).reversed())
                .collect(Collectors.toList());
    }

    public static void readyAndSchedule(Process process) {
        ready(process);
        schedule();
    }

    public static void killAndSchedule(Process process) {
        process.kill();
        schedule();
    }

    public static void wait(Process process) {
        waitingList.add(process);
        process.setState(ProcessState.WAITING);
        System.out.println("[" + process.getCodeFile().getFileName() + "] " + "进程进入 Waiting 状态");
        waitingList = waitingList.stream()
                .sorted(Comparator.comparing(Process::getSize).reversed())
                .collect(Collectors.toList());
        schedule();
    }

    private synchronized static void schedule() {
        if (readyList.isEmpty()) {
            runningProcess = null;
            return;
        }
        Process process = readyList.get(0);
        if (process.getSwap()) {
            int i = 0;
            while (i < 10 && !swapIn(process)) {
                swapOut();
                i++;
                if (i == 9) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            System.out.println("内存已满，十秒后将自动重试");
                        }
                    }, 10000);
                    i = 0;
                }
            }
            process.setSwap(false);
        }
        readyList.remove(process);
        process.setState(ProcessState.RUNNING);
        System.out.println("[" + process.getCodeFile().getFileName() + "] " + "进程进入 Running 状态");
        runningProcess = process;
        new Actuator(process).start();
        readyList.forEach(Process::incPriority);
    }

    public static void swapOut() {
        if (waitingList.isEmpty() && readyList.isEmpty()) {
            return;
        }
        Process process = null;
        if(waitingList.isEmpty()){
            for(int i = 0; i< readyList.size(); i++) {
                process = readyList.get(i);
                if (!process.getSwap()) {
                    break;
                }
            }
        } else {
            for(int i = 0; i< waitingList.size(); i++) {
                process = waitingList.get(i);
                if (!process.getSwap()) {
                    break;
                }
            }
        }
        process.setSwap(true);
        System.out.println("[" + process.getCodeFile().getFileName() + "] " + "进程换出");
        Global.memory.closeFile(process.getCodeFile());
        process.getOpenedFiles().forEach(Global.memory::closeFile);
    }

    public static boolean swapIn(Process process) {
        if (!Global.memory.loadFile(process.getCodeFile())) {
            return false;
        }
        for (File file : process.getOpenedFiles()) {
            if (!Global.memory.loadFile(file)) {
                Global.memory.closeFile(process.getCodeFile());
                process.getOpenedFiles().forEach(Global.memory::closeFile);
                return false;
            }
        }
        System.out.println("[" + process.getCodeFile().getFileName() + "] " + "进程换入");
        return true;
    }

    public static Process getRunningProcess() {
        return runningProcess;
    }

    public static void shutDown(Process process) {
        process.kill();
    }
}
