package com.bjdkf.os.core.process;

import java.util.ArrayList;
import java.util.Arrays;

public class CodeInterpreter {
    public CodeInterpreter() {}

    public static ArrayList<String> interpreting(String order)
    {
        String raw_string=order.trim();
        ArrayList<String> wronglist = new ArrayList<String>();
        if(raw_string.charAt(0)=='M') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 2) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if (raw_string.charAt(0)=='Y') {
                String[] orderlist = order.split(" ");
                if (orderlist.length == 2) {
                    ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                    return resultlist;
                } else
                    return wronglist;
        }
        else if   (raw_string.charAt(0)=='S') {
                String[] orderlist = order.split(" ");
                if (orderlist.length == 2) {
                    ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                    return resultlist;
                } else
                    return wronglist;
        }
        else if   (raw_string.charAt(0)=='R') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 3) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if   (raw_string.charAt(0)=='W') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 4) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if   (raw_string.charAt(0)=='D') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 3) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if   (raw_string.charAt(0)=='P') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 3) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if   (raw_string.charAt(0)=='C') {
            String[] orderlist = order.split(" ");
            if (orderlist.length == 3) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else if   (raw_string.charAt(0)=='Q'){
            String[] orderlist = order.split(" ");
            if (orderlist.length == 1) {
                ArrayList<String> resultlist = new ArrayList<String>(Arrays.asList(orderlist));
                return resultlist;
            } else
                return wronglist;
        }
        else{
            return wronglist;
        }
    }
}
