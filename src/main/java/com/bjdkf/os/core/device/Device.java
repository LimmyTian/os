package com.bjdkf.os.core.device;

import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.core.process.Scheduler;
import com.bjdkf.os.glb.Global;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Semaphore;


/**
 * @author Limmy
 * @date 2019-08-29 11:00
 */
public class Device {

    private String name;
    private int totalCount;
    private final Queue<Process> waitingQueue = new ConcurrentLinkedDeque<Process>();
    private Semaphore semp;

    private Device() {}

    public Device(String name, int totalCount) {
        this.name = name;
        this.totalCount = totalCount;
        this.semp = new Semaphore(totalCount);
        Global.DEVICES.put(name, this);
    }

    public int getFreeNum() {
        return semp.availablePermits();
    }

    /**
     * process 请求分配当前设备
     */
    public synchronized boolean aquire(Process process) {
        if (semp.tryAcquire()) {
            process.useDevice(this);
            return true;
        }
        waitingQueue.offer(process);
//        System.out.println("[Device] No valuable " + name + "could be used");
        return false;
    }

    /**
     * 释放当前设备
     */
    public synchronized void release() {
        if (waitingQueue.isEmpty()) {
            semp.release();
            return;
        }
        Process process = waitingQueue.poll();
        System.out.println("XXXXXXXXXXXXX");
        process.useDevice(this);
        Scheduler.ready(process);
    }

    public int getTotalCount() {
        return totalCount;
    }

    public Queue<Process> getWaitingQueue() {
        return waitingQueue;
    }

    public String getName() {
        return name;
    }
}
