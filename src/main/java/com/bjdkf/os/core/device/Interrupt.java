package com.bjdkf.os.core.device;

import com.bjdkf.os.core.process.Process;

/**
 * @author lijundi
 * @date 2019/9/2 9:51
 */
public class Interrupt {
    private Device device;
    private Process process;

    public Interrupt(Device device, Process process){
        this.device = device;
        this.process = process;
    }

    public Device getDevice() {
        return device;
    }

    public Process getProcess() {
        return process;
    }
}
