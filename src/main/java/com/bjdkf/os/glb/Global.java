package com.bjdkf.os.glb;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.device.Interrupt;
import com.bjdkf.os.core.file.Storage;
import com.bjdkf.os.core.memory.Memory;
import com.bjdkf.os.core.process.Process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Limmy
 * @date 2019-08-29 12:51
 */
public class Global {
    /**
     * 维护所有设备信息
     */
    public static final Map<String, Device> DEVICES = new HashMap<String, Device>();

    /**
     * 时间片大小
     */
    public static int duration;

    /**
     * 存储器实例
     */
    public static Storage storage;

    /**
     * 内存实例
     */
    public static Memory memory;

    /**
     * 进程队列
     */
    public static Map<Integer, Process> processes = new HashMap<Integer, Process>();

    /**
     * 中断表
     */
    public static ArrayList<Interrupt> interruptArrayList = new ArrayList<Interrupt>();

    /**
     * 满缓冲项默认初始值
     */
    public static int full = 0;

    /**
     * 空缓冲项默认初始值
     */
    public static int empty = 5;
}
