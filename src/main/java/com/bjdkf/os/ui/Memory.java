package com.bjdkf.os.ui;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class Memory extends JFrame {
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private String[] title={"已使用页数","未使用页数"};
    public boolean first1=true;
    public Memory(){
//        System.out.println("row");
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        jmodel=new DefaultTableModel(rowmessage,title);
        JTable jtable=new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refresh();
//        this.setContentPane(contentPane);
//        scrollPane=new JScrollPane();
//        contentPane.add(scrollPane,BorderLayout.CENTER);

        this.setTitle("内存情况");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);
    }
    public void refresh(){

        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        Integer memory=0;
        Integer free=0;
        memory=Global.memory.getBusyPagesSize();
        free=Global.memory.getFreePagesSize();
        String process_meory=Integer.toString(memory);
        String free_meory=Integer.toString(free);
        jmodel.addRow(new String[] {process_meory,free_meory});
        if(first1)
        {
            this.setVisible(true);
            first1=false;
        }
    }
}
