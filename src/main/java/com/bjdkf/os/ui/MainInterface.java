package com.bjdkf.os.ui;

import com.bjdkf.os.glb.Global;
import org.jline.builtins.Completers;
import org.jline.reader.*;
import org.jline.reader.impl.completer.AggregateCompleter;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import com.bjdkf.os.core.file.Storage;

import java.io.IOException;

public class MainInterface {
//    public Interpreter Interpreter;
//    public Scanner scanner=new Scanner(System.in);
//    public String getInput(){
//        System.out.print("输入指令:");
//        String input=scanner.nextLine();
//        return input;
//    }
    public MainInterface(){

    }

    public static void run() throws IOException{
        Terminal terminal = (Terminal) TerminalBuilder.builder()
                .system(true)
                .build();

        Completer lsCompleter=new ArgumentCompleter(
                new StringsCompleter("ls"),
                NullCompleter.INSTANCE
        );

        Completer mkdirCompleter=new ArgumentCompleter(
                new StringsCompleter("mkdir"),
                new Completers.FileNameCompleter(),
                NullCompleter.INSTANCE
        );

        Completer mkCompleter=new ArgumentCompleter(
                new StringsCompleter("mk"),
                new Completers.FileNameCompleter(),
                NullCompleter.INSTANCE
        );

        Completer rmCompleter=new ArgumentCompleter(
                new StringsCompleter("rm"),
                new Completers.FileNameCompleter(),
                NullCompleter.INSTANCE
        );

        Completer rmdirCompleter=new ArgumentCompleter(
                new StringsCompleter("rmdir"),
                new Completers.FileNameCompleter(),
                NullCompleter.INSTANCE
        );

        Completer cdBackCompleter=new ArgumentCompleter(
                new StringsCompleter("cd"),
                new StringsCompleter(".."),
                NullCompleter.INSTANCE
        );

        Completer cdDirlCompleter=new ArgumentCompleter(
                new StringsCompleter("cd"),
                new StringsCompleter("dirl"),
                NullCompleter.INSTANCE
        );

        Completer cdRootCompleter=new ArgumentCompleter(
                new StringsCompleter("cd"),
                new StringsCompleter("/"),
                NullCompleter.INSTANCE
        );

        Completer topCompleter=new ArgumentCompleter(
                new StringsCompleter("top"),
                NullCompleter.INSTANCE
        );

        Completer runCompleter=new ArgumentCompleter(
                new StringsCompleter("run"),
                new Completers.FileNameCompleter(),
                NullCompleter.INSTANCE
        );

        Completer shutDownCompleter=new ArgumentCompleter(
                new StringsCompleter("shutdown"),
                NullCompleter.INSTANCE
        );

        Completer osCompleter=new AggregateCompleter(
                lsCompleter,
                mkCompleter,
                mkdirCompleter,
                rmCompleter,
                rmdirCompleter,
                cdBackCompleter,
                cdDirlCompleter,
                cdRootCompleter,
                topCompleter,
                runCompleter,
                shutDownCompleter
        );

        LineReader lineReader = LineReaderBuilder.builder()
                .terminal(terminal)
                .completer(osCompleter)
                .history(new OsHistory())
                .build();


        Interpreter os_interpreter=new Interpreter();
        while (true) {
            String line;
            try {
                String address= Global.storage.getCurrentAddress();
                String prompt=address;
                line=lineReader.readLine(prompt);
                line.trim();
                os_interpreter.deal(line);
            }catch (UserInterruptException e) {

            }catch (EndOfFileException e) {
                System.out.println("\nBye");
                return;
            }
        }

    }
}
