package com.bjdkf.os.ui;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class ProcessUi extends JFrame {
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private String[] title={"名称","pid","状态","内存","会否换出"};
    public boolean first1=true;
    public ProcessUi(){
//        System.out.println("row");
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        jmodel=new DefaultTableModel(rowmessage,title);
        JTable jtable=new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        Boolean first=true;
        this.refresh();
//        this.setContentPane(contentPane);
//        scrollPane=new JScrollPane();
//        contentPane.add(scrollPane,BorderLayout.CENTER);
        jtable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2)
                {
                    int  row =((JTable)e.getSource()).rowAtPoint(e.getPoint()); //获得行位置
//                    System.out.println(row);
                    String processstr=(String)(jtable.getValueAt(row,1));
                    System.out.println(processstr);
                    ProcessUI_son son=new ProcessUI_son(processstr);
                    ProcessUi_grandson grandson=new ProcessUi_grandson(processstr);
                    Integer pid=Integer.parseInt(processstr);
                    Timer timer=new Timer(1000,new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            Process test=Global.processes.get(pid);
                            if(test!=null)
                            {
                                List<Device> devicelist=test.getAllocatedDevice();
                                List<File> filelist=test.getOpenedFiles();
                                son.refresh(devicelist);
                                grandson.refreshfile(filelist);
                            }
                            else {
                                son.dispose();
                                grandson.dispose();
                            }


                        }
                    });
                    timer.start();
                    //int  col=((JTable)e.getSource()).columnAtPoint(e.getPoint()); //获得列位置 String cellVal=(String)(tbModel.getValueAt(row,col)); //获得点击单元格数据 txtboxRow.setText((row+1)+""); txtboxCol.setText((col+1)+"");
                }
            }
        });
        this.setTitle("进程情况");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);

    }
    public void refresh(){

        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(Process singleprocess:Global.processes.values()){
            String progress_name=singleprocess.getCodeFile().getFileName();
            //             System.out.println(device_name);
            String process_pid=Integer.toString(singleprocess.getPid());
            String process_staus=singleprocess.getState().toString();
            Integer memory=0;
            List<File> process_file=singleprocess.getOpenedFiles();


            String process_change;
            if(singleprocess.getSwap())
            {
                process_change="是";

            }
            else
            {
                process_change="否";
                for(File process:process_file)
                {
                    memory+=process.getMemoryPages().size();
                }
                memory+=singleprocess.getCodeFile().getMemoryPages().size();
            }
            String process_meory=Integer.toString(memory);
            jmodel.addRow(new String[] {progress_name,process_pid,process_staus,process_meory,process_change});

        }
        if(first1)
        {
            this.setVisible(true);
            first1=false;
        }
    }
}
