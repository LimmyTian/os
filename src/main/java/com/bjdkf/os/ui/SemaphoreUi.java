package com.bjdkf.os.ui;

import com.bjdkf.os.core.process.Synchronizator;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Timer;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class SemaphoreUi extends JFrame  {
    private boolean first=true;
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private Timer timer;
    //    private JTable jtable;
    private String[] title={"名称","full","empty"};
    public SemaphoreUi(){
//        System.out.println("row");
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        jmodel=new DefaultTableModel(rowmessage,title);
        JTable jtable= new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refresh();
//        this.setContentPane(contentPane);
//        scrollPane=new JScrollPane();
//        contentPane.add(scrollPane,BorderLayout.CENTER);
//        jtable.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                if(e.getClickCount() == 2)
//                {
//                    int  row =((JTable)e.getSource()).rowAtPoint(e.getPoint()); //获得行位置
////                    System.out.println(row);
//                    String devicestr=(String)(jtable.getValueAt(row,0));
//                    deviceUI_son this_son=new deviceUI_son(devicestr);
//
//                    Timer timer=new Timer(1000,new ActionListener() {
//                        public void actionPerformed(ActionEvent evt) {
//                            Queue<Process> devicelist=  Global.DEVICES.get(devicestr).getWaitingQueue();
//                            this_son.refresh(devicelist);
//                        }
//                    });
//                    timer.start();
//                    //int  col=((JTable)e.getSource()).columnAtPoint(e.getPoint()); //获得列位置 String cellVal=(String)(tbModel.getValueAt(row,col)); //获得点击单元格数据 txtboxRow.setText((row+1)+""); txtboxCol.setText((col+1)+"");
//                }
//            }
//        });
//        scrollPane = new JScrollPane(jtable);


//        this.add(scrollPane);

        this.setTitle("信号量查看");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);
        this.setVisible(true);
    }
    public void refresh(){
        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(Map.Entry<String, List<Semaphore>> entry:Synchronizator.getSemaphoreMap().entrySet()){
            String sem_name=entry.getKey();
            String full=Integer.toString((entry.getValue().get(0)).availablePermits());
            String empty=Integer.toString((entry.getValue().get(1)).availablePermits());
            jmodel.addRow(new String[] {sem_name,full,empty});

        }
        if(first)
        {
            this.setVisible(true);
            first=false;
        }
    }

}
