package com.bjdkf.os.ui;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

public class ProcessUI_son extends JFrame {
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private String[] title={"设备列表"};
    public boolean first=true;
    public ProcessUI_son(String processstr){
//        System.out.println(devicestr);
//        System.out.println(Global.DEVICES);
        Integer pid=Integer.parseInt(processstr);

        List<Device> devicelist=Global.processes.get(pid).getAllocatedDevice();
//        System.out.println(devicelist);
        jmodel=new DefaultTableModel(rowmessage,title);

        JTable jtable=new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refresh(devicelist);
        String name=Global.processes.get(pid).getCodeFile().getFileName();
        this.setTitle(name+"进程详细信息");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 400, 400);
        //this.setVisible(true);
    }
    public void refresh(List<Device> devicelist){
        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(Device device:devicelist){
            String name=device.getName();
            jmodel.addRow(new String[] {name});

        }
        if(first)
        {
            this.setVisible(true);
            first=false;
        }
    }


}
