package com.bjdkf.os.ui;

import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.Queue;

public class deviceUI_son extends JFrame{
    private boolean first=true;
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private String[] title={"进程id","进程名称","进程状态"};
    public deviceUI_son(String devicestr){
//        System.out.println(devicestr);
//        System.out.println(Global.DEVICES);
        Queue<Process> devicelist=  Global.DEVICES.get(devicestr).getWaitingQueue();
//        System.out.println(devicelist);
        jmodel=new DefaultTableModel(rowmessage,title);

        JTable jtable=new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refresh(devicelist);
        String name=Global.DEVICES.get(devicestr).getName();
        this.setTitle(name+"设备进程队列");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 400, 400);
        this.setVisible(true);
    }
    public void refresh(Queue<Process> devicelist){
        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(Process process:devicelist){
            String process_id=Integer.toString(process.getPid());
            String process_name=process.getCodeFile().getFileName();
//             System.out.println(device_name);
            String process_state=process.getState().toString();
            jmodel.addRow(new String[] {process_id,process_name,process_state});
        }
        if(first) {
            this.setVisible(true);
            first=false;
        }
    }
}
