package com.bjdkf.os.ui;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

public class ProcessUi_grandson extends JFrame {
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private String[] title={"文件列表"};
    public boolean first=true;
    public ProcessUi_grandson(String processstr){
//        System.out.println(devicestr);
//        System.out.println(Global.DEVICES);
        Integer pid=Integer.parseInt(processstr);
        List<File> filelist=Global.processes.get(pid).getOpenedFiles();
//        System.out.println(devicelist);
        jmodel=new DefaultTableModel(rowmessage,title);

        JTable jtable=new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refreshfile(filelist);
        String name=Global.processes.get(pid).getCodeFile().getFileName();
        this.setTitle(name+"进程详细信息");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(400, 54, 400, 400);
        //this.setVisible(true);
    }

    public void refreshfile(List<File> filelist){
        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(File file:filelist){
            String name=file.getFileName();
            jmodel.addRow(new String[] {name});

        }
        if(first)
        {
            this.setVisible(true);
            first=false;
        }
    }

}
