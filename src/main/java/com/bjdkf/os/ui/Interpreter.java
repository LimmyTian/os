package com.bjdkf.os.ui;

import com.bjdkf.os.config.Config;
import com.bjdkf.os.core.file.Directory;
import com.bjdkf.os.core.file.File;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.core.process.Scheduler;
import com.bjdkf.os.glb.Global;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.*;

public class Interpreter {
    public Interpreter() {

    }

    public void deal(String input) {
        //命令ls
        String pattern_ls = "(\\s*)(ls)(\\s*)";
        //命令mkdir dir 创建一个叫做 'dir' 的目录'
        String pattern_mkdir = "(\\s*)(mkdir)(\\s+)(\\S+)(\\s*)";
        //-命令mk file 5 创建一个叫做 'file' 且大小为5页的文件
        String pattern_mk = "(\\s*)(mk)(\\s+)(\\S+)(\\s+)(\\d+)(\\s*)";
        //-命令 rm file 删除一个叫做 'file' 的文件
        String pattern_rm = "(\\s*)(rm)(\\s+)(\\S+)(\\s*)";
        //- 命令rmdir dir 删除一个叫做 'dir' 的目录
        String pattern_rmdir = "(\\s*)(rmdir)(\\s+)(\\S+)(\\s*)";
//        - cd 切换工作路径
//                - cd ..  返回上一级目录
//                - cd dir1 进入当前目录下的子目录
//                - cd /  返回根目录
        String pattern_cd_back = "(\\s*)(cd)(\\s+)(\\.\\.)(\\s*)";
        String pattern_cd_open = "(\\s*)(cd)(\\s+)([^\\.\\f\\n\\r\\t\\v/]+)(\\s*)";
        String pattern_cd_root = "(\\s*)(cd)(\\s+)(/)(\\s*)";
        //- top 动态实时显示cpu、内存、进程等使用情况
        String pattern_top = "(\\s*)(top)(\\s*)";
        //- run file 运行当前目录下一个叫做“file”的程序
        String pattern_run = "(\\s*)(run)(\\s+)(\\S+)(\\s*)";
        //- shutdown 关机
        String pattern_shutdown = "(\\s*)(shutdown)(\\s*)";

        //创建Pattern对象
        Pattern r_ls = Pattern.compile(pattern_ls);

        Pattern r_mkdir = Pattern.compile(pattern_mkdir);

        Pattern r_mk = Pattern.compile(pattern_mk);

        Pattern r_rm = Pattern.compile(pattern_rm);

        Pattern r_rmdir = Pattern.compile(pattern_rmdir);

        Pattern r_cd_back = Pattern.compile(pattern_cd_back);

        Pattern r_cd_open = Pattern.compile(pattern_cd_open);

        Pattern r_cd_root = Pattern.compile(pattern_cd_root);

        Pattern r_top = Pattern.compile(pattern_top);

        Pattern r_run = Pattern.compile(pattern_run);

        Pattern r_shutdown = Pattern.compile(pattern_shutdown);

        //创建Matcher对象
        Matcher m_ls = r_ls.matcher(input);

        Matcher m_mkdir = r_mkdir.matcher(input);

        Matcher m_mk = r_mk.matcher(input);

        Matcher m_rm = r_rm.matcher(input);

        Matcher m_rmdir = r_rmdir.matcher(input);

        Matcher m_cd_back = r_cd_back.matcher(input);

        Matcher m_cd_open = r_cd_open.matcher(input);

        Matcher m_cd_root = r_cd_root.matcher(input);

        Matcher m_top = r_top.matcher(input);

        Matcher m_run = r_run.matcher(input);

        Matcher m_shutdown = r_shutdown.matcher(input);

        if (m_ls.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            // 打印文件
            ArrayList<File> fileArrayList = currentDirectory.getFileList();
            // 打印目录
            ArrayList<Directory> directoryArrayList = currentDirectory.getDirectoryList();
            System.out.println("File:");
            fileArrayList.forEach(item -> System.out.println("- " + item.getFileName()));
            System.out.println("Directory");
            directoryArrayList.forEach((item -> System.out.println("- " + item.getDirectoryName())));
        } else if (m_mkdir.find()) {
            Global.storage.createDirectory(m_mkdir.group(4));

        } else if (m_mk.find()) {
            Global.storage.createFile(m_mk.group(4), Integer.parseInt(m_mk.group(6)));

        } else if (m_rm.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            // 打印文件
            File file = currentDirectory.getFile(m_rm.group(4));
            if (file != null) {
                Global.storage.delFile(m_rm.group(4));
            } else {
                System.out.println("该文件不存在");
            }

        } else if (m_rmdir.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            // 打印文件
            Directory directory = currentDirectory.getDirectory(m_rmdir.group(4));
            if (directory != null) {
                Global.storage.delDirectory(m_rmdir.group(4));
            } else {
                System.out.println("该目录不存在");
            }

        } else if (m_cd_back.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            Directory parentDirectory = currentDirectory.getParentDirectory();
            if (parentDirectory != null) {
                Global.storage.setCurrentDir(parentDirectory);
            } else {
                System.out.println("已在顶层目录");
            }

        } else if (m_cd_open.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            Directory directory = currentDirectory.getDirectory(m_cd_open.group(4));
            if (directory != null) {
                Directory childDirectory = currentDirectory.getDirectory(m_cd_open.group(4));
                Global.storage.setCurrentDir(childDirectory);
            } else {
                System.out.println("该目录不存在");
            }
        } else if (m_cd_root.find()) {
            Directory rootdirectory = Global.storage.getRootDir();
            Global.storage.setCurrentDir(rootdirectory);

        } else if (m_top.find()) {
            MainFramework test=new MainFramework();
        } else if (m_run.find()) {
            Directory currentDirectory = Global.storage.getCurrentDir();
            File file = currentDirectory.getFile(m_run.group(4));
            if (file != null) {
                try {
                    for (Process p:Global.processes.values()) {
                        if(p.getCodeFile().equals(file)){
                            System.out.println("该程序已运行");
                            throw new Exception("抛出异常");
                        }
                    }
                    java.io.File file1 = new java.io.File(file.getAddress());
                    BufferedReader br = new BufferedReader(new FileReader(file1));
                    String tmp = br.readLine();
                    tmp = br.readLine();
                    String[] tmp2 = tmp.split(" ");
                    Process newProcess = new Process(file, Integer.parseInt(tmp2[1]));
                    if (Scheduler.getRunningProcess() != null) {
                        Scheduler.ready(newProcess);
                    } else {
                        Scheduler.readyAndSchedule(newProcess);
                    }
                } catch (Exception e) {
                    System.out.println("程序运行失败");
                }
            } else {
                System.out.println("该文件不存在");
            }
        } else if (m_shutdown.find()) {
            Config.saveTotal();
            System.exit(0);
        } else {
            System.out.println("错误的指令");
        }
    }
}
