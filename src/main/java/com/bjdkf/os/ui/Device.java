package com.bjdkf.os.ui;


import com.bjdkf.os.ui.deviceUI;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.BorderLayout;
import java.util.concurrent.Semaphore;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

class MainFramework extends JFrame {

    public JPanel contentPane;
    public JScrollPane scrollPane;
    public JTextArea textArea;
    public JFrame jFrame;
    public MainFramework(){
        jFrame = new JFrame("设备管理器");
        jFrame.setSize(800, 500);
        jFrame.setDefaultCloseOperation(jFrame.HIDE_ON_CLOSE);// 设置关闭退出
        JMenuBar menuBar = new JMenuBar();
        jFrame.setJMenuBar(menuBar);
        JMenu menu1 = new JMenu("设备使用");
        JMenu menu2 = new JMenu("文件使用");
        JMenu menu3 = new JMenu("进程信息");
        JMenu menu4 = new JMenu("信号量查看");
        JMenu menu5 = new JMenu("内存使用");
        menuBar.add(menu1);
        menuBar.add(menu2);
        menuBar.add(menu3);
        menuBar.add(menu4);
        menuBar.add(menu5);
        JMenuItem divice=new JMenuItem("查看");
        JMenuItem file=new JMenuItem("查看");
        JMenuItem progress=new JMenuItem("查看");
        JMenuItem sempohre=new JMenuItem("查看");
        JMenuItem memory=new JMenuItem("查看");
        menu1.add(divice);
        menu2.add(file);
        menu3.add(progress);
        menu4.add(sempohre);
        menu5.add(memory);
        jFrame.setVisible(true);
        divice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deviceUI thisUI=new deviceUI();
                Timer timer=new Timer(1000,new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        thisUI.refresh();
                    }
                });
                timer.start();
            }
        });
        file.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Filee ui=new Filee();
                Timer timer=new Timer(1000,new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        ui.refresh();
                    }
                });
                timer.start();
            }
        });
        memory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Memory mui=new Memory();
                Timer timer=new Timer(1000,new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        mui.refresh();
                    }
                });
                timer.start();
            }
        });
        progress.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               ProcessUi processui=new ProcessUi();
                Timer timer=new Timer(1000,new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        processui.refresh();
                    }
                });
                timer.start();
            }
        });
        sempohre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SemaphoreUi semaphoreui=new SemaphoreUi();
                Timer timer=new Timer(1000,new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        semaphoreui.refresh();
                    }
                });
                timer.start();
            }
        });
    }


    public void file(){
        JPanel jp1,jp2;
        JLabel jb1,jb2;
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        this.setContentPane(contentPane);
        //scrollPane=new JScrollPane();
        //contentPane.add(scrollPane,BorderLayout.CENTER);

        jp1=new JPanel();
        jp2=new JPanel();
        jb1=new JLabel("已使用");
        jb2=new JLabel("未使用");
        jp1.add(jb1);
        jp2.add(jb2);

        //scrollPane.setViewportView(textArea);
        this.setLayout(new GridLayout(10,1));

        this.add(jp1);
        this.add(jp2);

        //this.add(scrollPane);
        this.setTitle("文件使用情况");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);
        this.setVisible(true);
    }


    public void progress(){
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        this.setContentPane(contentPane);
        scrollPane=new JScrollPane();
        contentPane.add(scrollPane,BorderLayout.CENTER);
        textArea=new JTextArea();
        scrollPane.add(textArea);
        scrollPane.setViewportView(textArea);
        this.setTitle("进程");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);
        this.setVisible(true);
    }

}

