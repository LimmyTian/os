package com.bjdkf.os.ui;

import org.jline.reader.impl.history.DefaultHistory;

import java.time.Instant;

public final class OsHistory extends DefaultHistory {
    private static boolean isComment(String line) {return  line.startsWith("#");}

    public  void add(Instant time,String line) {
        if (isComment(line)) {
            return;
        }
        super.add(time, line);
    }
}
