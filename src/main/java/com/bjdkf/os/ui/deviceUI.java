package com.bjdkf.os.ui;
import com.bjdkf.os.core.process.Synchronizator;
import com.bjdkf.os.ui.deviceUI_son;
import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.process.Process;
import com.bjdkf.os.glb.Global;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Timer;
import java.util.Map;
import java.util.Queue;

public class deviceUI extends JFrame  {
    private boolean first=true;
    private JPanel contentPane;
    private JScrollPane scrollPane;
    private DefaultTableModel jmodel;
    private String[][] rowmessage;
    private Timer timer;
//    private JTable jtable;
    private String[] title={"设备名称","总数量","已使用数量","未使用数量"};
    public deviceUI(){
//        System.out.println("row");
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        jmodel=new DefaultTableModel(rowmessage,title);
        JTable jtable= new JTable(jmodel);
        jtable.setEnabled(false);
        add(new JScrollPane(jtable));
        this.refresh();
//        this.setContentPane(contentPane);
//        scrollPane=new JScrollPane();
//        contentPane.add(scrollPane,BorderLayout.CENTER);
        jtable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2)
                {
                    int  row =((JTable)e.getSource()).rowAtPoint(e.getPoint()); //获得行位置
//                    System.out.println(row);
                    String devicestr=(String)(jtable.getValueAt(row,0));
                    deviceUI_son this_son=new deviceUI_son(devicestr);

                    Timer timer=new Timer(1000,new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            Queue<Process> devicelist=  Global.DEVICES.get(devicestr).getWaitingQueue();
                            this_son.refresh(devicelist);
                        }
                    });
                    timer.start();
                    //int  col=((JTable)e.getSource()).columnAtPoint(e.getPoint()); //获得列位置 String cellVal=(String)(tbModel.getValueAt(row,col)); //获得点击单元格数据 txtboxRow.setText((row+1)+""); txtboxCol.setText((col+1)+"");
                }
            }
        });
//        scrollPane = new JScrollPane(jtable);


//        this.add(scrollPane);

        this.setTitle("设备使用情况");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setBounds(0, 54, 800, 446);
        this.setVisible(true);
    }
    public void refresh(){
        for(int rownum=jmodel.getRowCount()-1;rownum>=0;rownum--)
            jmodel.removeRow(rownum);
        for(Device singledevice:Global.DEVICES.values()){
             String device_name=singledevice.getName();
//             System.out.println(device_name);
             String device_allnum=Integer.toString(singledevice.getTotalCount());
             String device_used=Integer.toString(singledevice.getTotalCount()-singledevice.getFreeNum());
             String device_free=Integer.toString(singledevice.getFreeNum());
             jmodel.addRow(new String[] {device_name,device_allnum,device_used,device_free});

         }
        if(first)
        {
            this.setVisible(true);
            first=false;
        }
    }

}
