package com.bjdkf.os.ds;

import java.util.ArrayList;

// Create by lijundi
// 2019/08/29
public class PageStore {
    private ArrayList<Page> freePages;
    private ArrayList<Page> busyPages;

    public PageStore(int pageNum) {
        this.freePages = new ArrayList<Page>();
        for (int i = 0; i < pageNum; i++) {
            Page page = new Page(i);
            this.freePages.add(page);
        }
        this.busyPages = new ArrayList<Page>();
    }

    public ArrayList<Page> loadBySize(int size) {
        if (size > this.freePages.size()) {
            return null;
        } else {
            ArrayList<Page> allocatedPages = new ArrayList<Page>();
            for (int i = 0; i < size; i++) {
                Page p = this.freePages.get(0);
                allocatedPages.add(p);
                this.busyPages.add(p);
                this.freePages.remove(p);
            }
            return allocatedPages;
        }
    }

    public void releasePage(ArrayList<Page> pages) {
        for (Page releasePage : pages) {
            for (Page page : this.busyPages) {
                if (releasePage.equals(page)) {
                    this.freePages.add(page);
                    this.busyPages.remove(page);
                    break;
                }
            }
        }
    }

    public int getBusyPagesSize() {
        return busyPages.size();
    }

    public int getFreePagesSize() {
        return freePages.size();
    }
}
