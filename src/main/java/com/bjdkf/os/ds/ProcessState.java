package com.bjdkf.os.ds;

import com.google.common.base.Enums;
/**
 * @author Limmy
 * @date 2019-08-29 13:25
 */
public enum ProcessState {
    // 新的
    NEW("new"),

    // 就绪
    READY("ready"),

    // 进行
    RUNNING("running"),

    // 等待
    WAITING("waiting"),

    // 终止
    ABORT("abort");

    private final String processState;

    ProcessState(String processState) {
        this.processState = processState;
    }

    public static ProcessState getIfPresent(final String processState) {
        if (processState == null) {
            return null;
        }
        return Enums.getIfPresent(ProcessState.class, processState.toUpperCase()).orNull();
    }

    public static boolean isValidProcessState(final String state) {
        return getIfPresent(state) != null;
    }

    @Override
    public String toString() {
        return this.processState;
    }
}
