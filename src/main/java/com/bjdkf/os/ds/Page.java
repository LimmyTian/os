package com.bjdkf.os.ds;

// Create by lijundi
// 2019/08/29
public class Page {
    private int pageId;

    public Page(int pageId) {
        this.pageId = pageId;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public void copy(Page page) {

    }
}
