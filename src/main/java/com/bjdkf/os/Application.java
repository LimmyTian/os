package com.bjdkf.os;

import com.bjdkf.os.config.Config;
import com.bjdkf.os.ui.MainInterface;
import java.io.IOException;


public class Application {
    public static void main(String[] args) {
        if (!Config.loadConfig()) {
            return;
        }
        /**
         * 启动命令行
         */
        MainInterface mainInterface=new MainInterface();
        try {
            mainInterface.run();
        }
        catch (IOException e){
            System.out.println("aaaa");

        }
    }
}
