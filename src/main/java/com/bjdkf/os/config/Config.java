package com.bjdkf.os.config;

import com.bjdkf.os.core.device.Device;
import com.bjdkf.os.core.file.Directory;
import com.bjdkf.os.core.file.Storage;
import com.bjdkf.os.core.memory.Memory;
import com.bjdkf.os.glb.Global;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * @author Limmy
 * @author lijundi
 * @date 2019-08-29 10:52
 */
public class Config {
    private static FileWriter fileFilesWriter;
    private static FileWriter fileDirectoriesWriter;

    public static boolean loadConfig() {
        return loadSettings() && loadDevice() && loadDirectory() && loadFile() && loadProgram();
    }

    private static boolean loadSettings() {
        try {
            File file = new File("./src/main/java/com/bjdkf/os/config/settings.conf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            Global.memory = new Memory(Integer.parseInt(br.readLine()));
            Global.storage = new Storage(Integer.parseInt(br.readLine()));
            Global.duration = Integer.parseInt(br.readLine());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean loadDevice() {
        try {
            File file = new File("./src/main/java/com/bjdkf/os/config/devices.conf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tmp = br.readLine();
            while (tmp != null) {
                String[] device = tmp.split(" ");
                Global.DEVICES.put(device[0], new Device(device[0], Integer.parseInt(device[1])));
                tmp = br.readLine();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean loadDirectory() {
        try {
            File file = new File("./src/main/java/com/bjdkf/os/config/directories.conf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tmp = br.readLine();
            while (tmp != null) {
                String[] directories = tmp.split("/");
                Global.storage.createDirectoryByAddress(directories);
                tmp = br.readLine();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean loadFile() {
        try {
            File file = new File("./src/main/java/com/bjdkf/os/config/files.conf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tmp = br.readLine();
            while (tmp != null) {
                String[] directories = tmp.split("/");
                Global.storage.createFileByAddress(directories);
                tmp = br.readLine();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean loadProgram() {
        try {
            File file = new File("./src/main/java/com/bjdkf/os/config/programs.conf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tmp = br.readLine();
            while (tmp != null) {
                String[] tmpList = tmp.split("/");
                //读程序文件
                File file2 = new File(tmp);
                BufferedReader br2 = new BufferedReader(new FileReader(file2));
                String tmp2 = br2.readLine();
                String[] tmpList2 = tmp2.split(" ");
                Global.storage.setCurrentDir(Global.storage.getRootDir().getDirectory("program"));
                Global.storage.createProgram(tmpList[tmpList.length - 1], Integer.parseInt(tmpList2[1]), tmp);
                Global.storage.setCurrentDir(Global.storage.getRootDir());
                tmp = br.readLine();
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean saveTotal() {
        try {
            Directory rootdirectory = Global.storage.getRootDir();
            File fileFiles = new File("./src/main/java/com/bjdkf/os/config/files.conf");
            fileFilesWriter = new FileWriter(fileFiles);
            fileFilesWriter.write("");
            fileFilesWriter.flush();
            File fileDirectories = new File("./src/main/java/com/bjdkf/os/config/directories.conf");
            fileDirectoriesWriter = new FileWriter(fileDirectories);
            fileDirectoriesWriter.write("");
            fileDirectoriesWriter.flush();
            Save(rootdirectory);
        } catch (Exception e) {
            return false;
        }
        return true;

    }

    private static boolean Save(Directory directory) {
        Global.storage.setCurrentDir(directory);
        ArrayList<Directory> directoryArrayList = directory.getDirectoryList();
        ArrayList<com.bjdkf.os.core.file.File> fileArrayList = directory.getFileList();
        fileArrayList.forEach(item -> saveFile(item, directory));
        if (!directoryArrayList.isEmpty()) {
            directoryArrayList.forEach(Config::Save);
        } else {
            saveDirectory(directory);
        }
        return true;
    }

    private static boolean saveFile(com.bjdkf.os.core.file.File mfile, Directory directory) {
        if (mfile.getType() != 1) {
            try {
                String address = "";
                Directory tmpDir = directory;
                while (!tmpDir.equals(Global.storage.getRootDir())) {
                    String tmpString = tmpDir.getDirectoryName() + "/";
                    address = tmpString.concat(address);
                    tmpDir = tmpDir.getParentDirectory();
                }
                fileFilesWriter.write(address.concat(mfile.getFileName() + " " + mfile.getSize() + "\n"));
                fileFilesWriter.flush();
            } catch (Exception e) {
                return false;
            }
            return true;
        }
        return false;
    }

    private static boolean saveDirectory(Directory directory) {
        if (directory.getParentDirectory() != null) {
            try {
                String address = directory.getDirectoryName();
                Directory tmpDir = directory.getParentDirectory();
                while (!tmpDir.equals(Global.storage.getRootDir())) {
                    String tmpString = tmpDir.getDirectoryName() + "/";
                    address = tmpString.concat(address);
                    tmpDir = tmpDir.getParentDirectory();
                }
                fileDirectoriesWriter.write(address + "\n");
                fileDirectoriesWriter.flush();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }


}
